
output jenkins_ip {
    description = "Jenkins url"
    value = format("http://%s:%s", aws_instance.jenkinsbuild-agent.public_ip, "8080")
} 