terraform {
  required_version = ">=0.14"

  backend "s3" {
    bucket         = "kojitechs.vpc.modules.trtr"
    dynamodb_table = "terraform-state-lock" # 
    region         = "us-east-1"
    key            = "path/env/terraform-pipeline"
    encrypt        = true
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "default"
}

