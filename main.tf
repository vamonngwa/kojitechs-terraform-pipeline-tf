
/*
module "vpc" {
  source = "git::https://github.com/Charles123Bob-Duru/kojitechs-vpc-module.git?ref=v1.1.0" 

  vpc_cidr      = "10.0.0.0/16"
  public_cidr   = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]
  private_cidr  = ["10.0.1.0/24", "10.0.3.0/24"]
  database_cidr = ["10.0.5.0/24", "10.0.7.0/24"]
  vpc_tags = {
    Name = "jenkinsbuild-agent-vpc"
  }
}
*/

### We are using terraform remote state datasource to pull down out network attr in this #{path.root}
data "terraform_remote_state" "operational_env" {
  backend = "s3"
  
  config = {
    region = "us-east-1"
    bucket = "kojitechs.vpc.modules.trtr"
    key = "path/env"
  }
}

locals {
   operational_env =  data.terraform_remote_state.operational_env.outputs
   vpc_id = local.operational_env.vpc_id
   public_subnet =  local.operational_env.public_subnet
   private_subnet =  local.operational_env.private_subnet
}

resource "aws_security_group" "jenkins-sg" {
  name        = "jenkins-sg"
  description = "Allow http from internet (route53)"
  vpc_id      = local.vpc_id

  
  ingress {
    description = "Allow http from internet (route53)"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "jenkins-sg"
  }
}

data "aws_ami" "ami" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-gp2"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "jenkinsbuild-agent" {
  ami           = data.aws_ami.ami.id
  instance_type = "t2.xlarge"
  subnet_id =  local.public_subnet[0]
  vpc_security_group_ids = [aws_security_group.jenkins-sg.id]
  user_data = file("${path.module}/template/jenkins.sh")
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name

  tags = {
    Name = "jenkinsbuild-agent"
  }
}
